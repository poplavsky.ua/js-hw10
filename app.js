/*
Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:
У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.
*/

function openName(evt, tabName) {

    let i, tabcontent, tablinks;

    tabcontent = document.getElementsByClassName("tabs-item");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tabs-title");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
  }